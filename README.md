# HomeworkTwo

# Инструменты
+ Linux Ubuntu 18.04
+ InteliJ IDEA
+ JDK 1.8
+ Maven
+ Hadoop 2.8.5
+ Spark
+ Sqoop
+ MySql

# Скрипты
createRecord.py - скрипт, для создания записей и загрузки в mysql.

load_to_HDFS.py - скрипт, для переноса данных из MySql в hdfs.

# Работа с Hadoop
## Установка
[Ссылка 1](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html#Pseudo-Distributed_Operation)

[Ссылка 2](https://linuxconfig.org/how-to-install-hadoop-on-ubuntu-18-04-bionic-beaver-linux)
# Работа с Spark
## Установка
[Ссылка 3](https://www.tutorialspoint.com/apache_spark/apache_spark_installation.htm)
# Работа с Sqoop
## Установка
[Ссылка 4](https://www.tutorialspoint.com/sqoop/sqoop_installation.htm)
#### Запуск служб для работы с hdfs
```
start-dfs.sh
start-yarn.sh
```
#### Остановка служб hfds
```
stop-dfs.sh
stop-yarn.sh
```
#### Загрузка файла
```
hadoop fs -put {path}
```
#### Чтение файла
```
hdfs dfs -text {path}
```
{path} - путь до файла.
# Запуск
```
spark-submit --class MainClass --master local target/HwTwo-1.0-SNAPSHOT.jar {inputFile} {outputFile} {aggregator}
```
##### Параметры:
{inputFile} - файл для чтения данных;
{outputFile} - файл для записи данных;
{aggregator} - значение из следующего списка: min, max, avg
##### Например:
```
spark-submit --class MainClass --master local target/HwTwo-1.0-SNAPSHOT.jar hdfs://localhost:9000/user/ramus/HW1/o/part-m-00000 hdfs://localhost:9000/user/ramus/HW1/oooo min
```
### Проверка сервера:
http://localhost:50070

http://localhost:8088

# Установка проекта
Открываем командную строку в папке с файлов pom.xml и в командной строке пишем
```
mvn clean install
```



