import managers.SparkManager;

public class MainClass {


    public static void main(String[] args) {

        // args[] - "hdfs://localhost:9000/user/input.txt"
        // args[] - "hdfs://localhost:9000/user/input.txt"
        // args[] - min or max or avg
        /*System.out.println(args[0]);
        System.out.println(args[1]);
        System.out.println(args[2]);*/

        if ((args.length!=3) || (!(args[2].equals("avg") || args[2].equals("min") || args[2].equals("max")))) {
            System.err.println("Usage: JavaAggregator <input file> <scale>\n" +
                    "Files must be into HDFS\n" +
                    "Scale must be only:\n" +
                    "- avg - average metric\n" +
                    "- min - minimum metric\n" +
                    "- max - maximum metric");
            System.exit(1);
        }

        SparkManager sparkManager = new SparkManager(args);
        sparkManager.start();
        sparkManager.stop();

    }
}
