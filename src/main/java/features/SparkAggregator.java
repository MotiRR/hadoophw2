package features;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;

import java.util.Date;

public class SparkAggregator {

    public Dataset<String> aggregate(Dataset<Row> metricDF, Dataset<String> sendDataSet, String operationAggregate) {

        switch (operationAggregate) {
            case "avg":
                sendDataSet = metricDF.groupBy("id").agg(functions.round(functions.avg("value").as("average"), 2)).
                        orderBy(functions.asc("id")).map(
                        (MapFunction<Row, String>)
                                row -> row.get(0) + ", " + Long.toString(new Date().getTime()) + ", avg, " + row.get(1),
                        Encoders.STRING());
                break;
            case "min":
                sendDataSet = metricDF.groupBy("id").agg(functions.min("value").as("minimum")).
                        orderBy(functions.asc("id")).map(
                        (MapFunction<Row, String>)
                                row -> row.get(0) + ", " + Long.toString(new Date().getTime()) + ", min, " + row.get(1),
                        Encoders.STRING());
                break;
            case "max":
                sendDataSet = metricDF.groupBy("id").agg(functions.max("value").as("maximum")).
                        orderBy(functions.asc("id")).map(
                        (MapFunction<Row, String>)
                                row -> row.get(0) + ", " + Long.toString(new Date().getTime()) + ", max, " + row.get(1),
                        Encoders.STRING());
                break;
        }

        return sendDataSet;
    }
}
