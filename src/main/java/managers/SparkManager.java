package managers;

import features.SparkAggregator;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;

public class SparkManager {

    SparkSession spark;
    Dataset<Row> metricDF;
    JavaRDD<Metric> metricRDD;
    Dataset<String> sendDataSet;
    String pathRead, pathWrite, operationAggregate;
    SparkAggregator aggregator;

    public SparkManager(String[] args) {
        // create a basic SparkSession
        spark = SparkSession
                .builder()
                .appName("Java Spark SQL")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        pathRead = args[0];
        pathWrite = args[1];
        operationAggregate = args[2];
        aggregator = new SparkAggregator();
    }

    public void start() {
        readRDD();
        createDataFrame();
        createDataSet();
        writeDataSet();
    }

    // stop spark session application
    public void stop() {
        spark.stop();
    }

    private void readRDD() {
        metricRDD = spark.read()
                .textFile(pathRead)
                .javaRDD()
                .map(line -> {
                            String[] parts = line.split(",");
                            Metric metric = new Metric();
                            metric.setId(Integer.parseInt(parts[0].trim()));
                            metric.setValue(Integer.parseInt(parts[2].trim()));
                            return metric;
                        }
                );
    }


    private void createDataFrame() {
        // Apply a schema to an RDD of JavaBeans to get a DataFrame
        metricDF = spark.createDataFrame(metricRDD, Metric.class);
        // Register the DataFrame as a temporary view
        metricDF.createOrReplaceTempView("metric");
    }

    private void createDataSet() {
        // prepare DataSet variable
        sendDataSet = spark.createDataset(new ArrayList<String>(), Encoders.STRING());
    }

    // write data set in hdfs
    private void writeDataSet() {
        aggregator.aggregate(metricDF, sendDataSet, operationAggregate)
                .write()
                .format("text")
                .save(pathWrite);
    }
}
