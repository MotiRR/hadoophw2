package managers;

import java.io.Serializable;

public class Metric implements Serializable {
    private int id;
    private int value;

    // get id method
    public int getId() {
        return id;
    }

    // set id method
    public void setId(int id) {
        this.id = id;
    }

    // get value method
    public int getValue() {
        return value;
    }

    // set value method
    public void setValue(int value) {
        this.value = value;
    }
}