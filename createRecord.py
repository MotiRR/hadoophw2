import pymysql
import time
from pymysql.cursors import DictCursor
from random import randrange

def createConnection():
 connection = pymysql.connect(
    host='localhost',
    user='ramus',
    password='1234',
    db='HWTWO',
    #charset='utf8mb4',
    cursorclass=DictCursor
 )
 return connection

if __name__ == "__main__":

 count = 100
 connect = createConnection()

 try:
  with connect.cursor() as cursor:
   for i in range(count):
    query = "INSERT INTO metrics VALUES (%s, %s, %s)"
    cursor.execute(query, (randrange(0,256,1), str(int(time.time())), randrange(1,500001,1)))
    connect.commit()
 finally:
  connect.close()
